#!/bin/bash

#################################
# Installation script for new Rasperry Pi devices
# Every action is logged to /root/firstinstall.log
# 1. Applications and system update/upgrade 
# 2. Editing SSH config for allow remote root login
# 3. Creating kiosk user with kiosk password
# 4. Installing samba with /home/kiosk as shared directory. Only kiosk user can log into samba repository
# 5. Creating virtualenv for PT Project and install requirements
# 6. Creating service for autostart manage.py runserver
# 7. Creating service for autostart chromium in kiosk mode
#################################

#Checking if root user
if [ $EUID -ne 0 ]; then
	echo "You need to be root to perform this operations"
	exit $E_NOTROOT
fi

apt-get -y update
apt-get -y upgrade

touch /root/firstinstall.log #Creating logging file

cp /etc/ssh/sshd_config /etc/ssh/sshd_config.org
sed -i -e "s/#PermitRootLogin prohibit-password/PermitRootLogin yes/g" /etc/ssh/sshd_config #Root login allow get to raspbian via winscp
echo "$(date): Permitted SSH Root Login" > /root/firstinstall.log 2>&1
systemctl restart sshd.service

adduser --quiet --disabled-password --shell /bin/bash --home /home/kiosk  --gecos "kiosk" kiosk 
echo "kiosk:kiosk" | chpasswd

echo "$(date): Added kiosk user" >> /root/firstinstall.log 2>&1
echo "samba-common samba-common/workgroup string  WORKGROUP" | sudo debconf-set-selections #no pop-up message
echo "samba-common samba-common/dhcp boolean true" | sudo debconf-set-selections #no pop-up message
echo "samba-common samba-common/do_debconf boolean true" | sudo debconf-set-selections #no pop-up message
apt-get -y install samba samba-common-bin
(echo kiosk; echo kiosk) | smbpasswd -a -s kiosk
echo "$(date): Installed samba" >> /root/firstinstall.log 2>&1

mv /etc/samba/smb.conf /etc/samba/smb.conf.org
cat /root/smbconfig > /etc/samba/smb.conf #Preconfigured smbcofig file
echo "$(date): Configured samba" >> /root/firstinstall.log 2>&1
systemctl restart smbd.service

chown kiosk /PT-TablicaInformacyjna
chmod 755 /PT-TablicaInformacyjna
echo "$(date): Changed owner and access rights" >> /root/firstinstall.log 2>&1

apt-get install python3-pip
pip3 install virtualenv
python3 -m venv /PT-TablicaInformacyjna/pt
source /PT-TablicaInformacyjna/pt/bin/activate
pip install -r /PT-TablicaInformacyjna/requirements.txt
python /PT-TablicaInformacyjna/PTTablicaInformacyjna/manage.py migrate
python /PT-TablicaInformacyjna/PTTablicaInformacyjna/manage.py createsuperuser
echo "$(date): Installed dependencies from requirements.txt" >> /root/firstinstall.log 2>&1

cp /root/kiosk.sh /home/pi/kiosk.sh
chown pi /home/pi/kiosk.sh
chmod 755 /home/pi/kiosk.sh

touch /etc/systemd/system/ptserver.service
chown pi /etc/systemd/system/ptserver.service
chmod 755 /etc/systemd/system/ptserver.service
cat /root/ptserver > /etc/systemd/system/ptserver.service
systemctl daemon-reload
systemctl start ptserver.service
systemctl enable ptserver.service
echo "$(date): Created PT Django service" >> /root/firstinstall.log 2>&1

cp /root/chromeautostart.sh /home/pi/chromeautostart.sh
chown pi /home/pi/chromeautostart.sh
chmod 755 /home/pi/chromeautostart.sh
cp /root/chromeautostart /etc/systemd/system/chromeautostart.service
systemctl daemon-reload
systemctl start chromeautostart.service
systemctl enable chromeautostart.service
echo "$(date): Created Chrome Autostart service" >> /root/firstinstall.log 2>&1

sysctl -w net.ipv4.conf.eth0.route_localnet=1 #Allow redirect from eth interface to loopback
iptables -t nat -A PREROUTING -p tcp --dport 8000 -j DNAT --to-destination 127.0.0.1:8000 #Allow incoming 8000 port